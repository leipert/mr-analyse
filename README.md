# Analysis tool for gitlab merge requests

> This bash script gives you the possibility to analyse merge requests quickly.

- [Requirements && Running](#requirements--running)
- [Results](#results)
- [Pre-calculated results](#pre-calculated-results)
  * [Analysis of gitlab-ce!9546](#analysis-of-gitlab-ce9546)
  * [Analysis of gitlab-ce!10319](#analysis-of-gitlab-ce10319)
  * [Analysis of gitlab-ce!12069](#analysis-of-gitlab-ce12069)
  * [Analysis of gitlab-ce!13884](#analysis-of-gitlab-ce13884)

## Update 2017-08-09

Some improvements have been made to the initial render. See results below

Size:

| MR | Size `body` (2017-11-15) | Size `body` (2017-08-09) | Improvement |
| - | - | - | - |
| [gitlab-ce!8844] | 905.16 kilobyte | 72.65 kilobyte | 100% -> 8%  |
| [gitlab-ce!9546] | 3015.34 kilobyte | 90.32 kilobyte | 100% -> 2.9% |
| [gitlab-ce!10319] | 2779.94 kilobyte | 67.59 kilobyte | 100 -> 2.4% |
| [gitlab-ce!12069] | 5133.53 kilobyte | 109.24 kilobyte | 100% -> 2.2% |
| [gitlab-ce!13884] | 1492.45 kilobyte | 81.39 kilobyte | 100% -> 5.4% |

Time to first byte:

| MR | 2017-11-15 | 2017-08-09 | Improvement |
| - | - | - | - |
| [gitlab-ce!8844] | 7.06309 s | 2.2182 s | 100% -> 31%  |
| [gitlab-ce!9546] | 19.3692 s | 3.4009 s | 100% -> 17.5% |
| [gitlab-ce!10319] | 21.9077 s | 2.71679 s | 100% -> 12.4% |
| [gitlab-ce!12069] | 37.3437 s | 4.19198 s | 100% -> 11.2% |
| [gitlab-ce!13884] | 10.3551 s | 2.55488 s | 100% -> 24.7% |

## Requirements && Execution

- bash, curl and bc
- [pup](https://github.com/ericchiang/pup)

Clone this repo and run
```bash
# Check Requirements
which bash && which curl && which bc && which pup

./analysis.sh [url]
# e.g.
./analysis.sh https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/8844
```

The analysis results will be printed to your terminal and saved to the `result/` folder.

## Results

The result consists of three sections. The first section contains some basic information regarding the merge request:

```markdown
# Analysis of https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/8844
- File info: `-rw-r--r-- 1 leipert staff 963K Nov 15 19:56 html/gitlab-ce-8844.html`
- Average Time to first Byte: 7.06309 seconds
```

The second section contains a quick analysis of the content. The size of the body and a count of all notes is displayed, to give context for each of the three categories:

- System notes (commits, mentions, etc.)
- Discussion threads on code
- User comments

```markdown
- `<body>` size: 	905.16 kilobyte (100.00% of body) with 131 notes:
     - `ul#notes-list > li.system-note` size: 	202.54 kilobyte (22.00% of body) - 63 elements
     - `ul#notes-list > li.note-discussion` size: 	544.55 kilobyte (60.00% of body) - 34 elements
     - User Comments size: 	90.14 kilobyte (9.00% of body) - 34 elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)
```

The third section gives information regarding potential savings, by:
- only rendering three commits per commit list (at the moment all are rendered, but only a maximum of three are shown)
- not including the discussion body of resolved discussions on load

```markdown
## Potential savings:
- Only rendering 3 commits per commit list
    ... would only render a maximum 102 of 142 commits, saving 14.06 kilobyte (6.00% of all `ul#notes-list > li.system-note`)
- Only rendering unresolved discussions (`ul#notes-list > li.note-discussion .discussion-body:not(.hide)`)...
    ... would leave 31 of 34 unrendered, saving 403.41 kilobyte (74.00% of all `ul#notes-list > li.note-discussion`)
```

## Pre-calculated results

Below you can find the pre-calculated results of 5 merge requests:

- [gitlab-ce!9546]
- [gitlab-ce!10319]
- [gitlab-ce!12069]
- [gitlab-ce!13884]
- [gitlab-ce!14882]


### Analysis of [gitlab-ce!9546]

- Link: [gitlab-ce!9546]
- File info: `-rw-r--r-- 1 leipert staff 3.2M Nov 15 19:56 html/gitlab-ce-9546.html`
- Average Time to first Byte: 19.3692 seconds

* * *

- `<body>` size: 	3015.34 kilobyte (100.00% of body) with 378 notes:
     - `ul#notes-list > li.system-note` size: 	991.80 kilobyte (32.00% of body) - 138 elements
     - `ul#notes-list > li.note-discussion` size: 	1467.00 kilobyte (48.00% of body) - 72 elements
     - User Comments size: 	450.95 kilobyte (14.00% of body) - 168 elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)

* * *

#### Potential savings:
- Only rendering 3 commits per commit list
    ... would only render a maximum 207 of 1676 commits, saving 507.07 kilobyte (51.00% of all `ul#notes-list > li.system-note`)
- Only rendering unresolved discussions (`ul#notes-list > li.note-discussion .discussion-body:not(.hide)`)...
    ... would leave 68 of 72 unrendered, saving 1382.61 kilobyte (94.00% of all `ul#notes-list > li.note-discussion`)

### Analysis of [gitlab-ce!10319]

- Link: [gitlab-ce!10319]
- File info: `-rw-r--r-- 1 leipert staff 2.9M Nov 15 19:37 html/gitlab-ce-10319.html`
- Average Time to first Byte: 21.9077 seconds

* * *

- `<body>` size: 	2779.94 kilobyte (100.00% of body) with 156 notes:
     - `ul#notes-list > li.system-note` size: 	1991.61 kilobyte (71.00% of body) - 88 elements
     - `ul#notes-list > li.note-discussion` size: 	596.65 kilobyte (21.00% of body) - 35 elements
     - User Comments size: 	124.68 kilobyte (4.00% of body) - 33 elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)

* * *

#### Potential savings:
- Only rendering 3 commits per commit list
    ... would only render a maximum 171 of 4118 commits, saving 1390.65 kilobyte (69.00% of all `ul#notes-list > li.system-note`)
- Only rendering unresolved discussions (`ul#notes-list > li.note-discussion .discussion-body:not(.hide)`)...
    ... would leave 35 of 35 unrendered, saving 596.65 kilobyte (100.00% of all `ul#notes-list > li.note-discussion`)


### Analysis of [gitlab-ce!12069]

- Link: [gitlab-ce!12069]
- File info: `-rw-r--r-- 1 leipert staff 5.3M Nov 15 19:56 html/gitlab-ce-12069.html`
- Average Time to first Byte: 37.3437 seconds

* * *

- `<body>` size: 	5133.53 kilobyte (100.00% of body) with 648 notes:
     - `ul#notes-list > li.system-note` size: 	1802.99 kilobyte (35.00% of body) - 369 elements
     - `ul#notes-list > li.note-discussion` size: 	3028.39 kilobyte (58.00% of body) - 198 elements
     - User Comments size: 	205.86 kilobyte (4.00% of body) - 81 elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)

* * *

#### Potential savings:
- Only rendering 3 commits per commit list
    ... would only render a maximum 459 of 2290 commits, saving 658.30 kilobyte (36.00% of all `ul#notes-list > li.system-note`)
- Only rendering unresolved discussions (`ul#notes-list > li.note-discussion .discussion-body:not(.hide)`)...
    ... would leave 196 of 198 unrendered, saving 3022.94 kilobyte (99.00% of all `ul#notes-list > li.note-discussion`)

### Analysis of [gitlab-ce!13884]

- Link: [gitlab-ce!13884]
- File info: `-rw-r--r-- 1 leipert staff 1.6M Nov 15 19:55 html/gitlab-ce-13884.html`
- Average Time to first Byte: 10.3551 seconds

* * *

- `<body>` size: 	1492.45 kilobyte (100.00% of body) with 208 notes:
     - `ul#notes-list > li.system-note` size: 	385.43 kilobyte (25.00% of body) - 133 elements
     - `ul#notes-list > li.note-discussion` size: 	972.44 kilobyte (65.00% of body) - 56 elements
     - User Comments size: 	44.76 kilobyte (2.00% of body) - 19 elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)

* * *

#### Potential savings:
- Only rendering 3 commits per commit list
    ... would only render a maximum 195 of 248 commits, saving 17.96 kilobyte (4.00% of all `ul#notes-list > li.system-note`)
- Only rendering unresolved discussions (`ul#notes-list > li.note-discussion .discussion-body:not(.hide)`)...
    ... would leave 56 of 56 unrendered, saving 972.44 kilobyte (100.00% of all `ul#notes-list > li.note-discussion`)

### Analysis of [gitlab-ce!14882]

- Link: [gitlab-ce!14882]
- File info: `-rw-r--r-- 1 leipert staff 156K Nov 15 19:55 html/gitlab-ce-14882.html`
- Average Time to first Byte: 2.54359 seconds

* * *

- `<body>` size: 	141.12 kilobyte (100.00% of body) with 9 notes:
     - `ul#notes-list > li.system-note` size: 	7.10 kilobyte (5.00% of body) - 3 elements
     - `ul#notes-list > li.note-discussion` size: 	62.06 kilobyte (43.00% of body) - 3 elements
     - User Comments size: 	8.68 kilobyte (6.00% of body) - 3 elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)

* * *

#### Potential savings:
- Only rendering 3 commits per commit list
    ... would have no effect, there less commits than (commit lists * 3) (0 < 0)
- Only rendering unresolved discussions (`ul#notes-list > li.note-discussion .discussion-body:not(.hide)`)...
    ... would have no effect, as there are no resolved discussions

[gitlab-ce!8844]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/8844
[gitlab-ce!9546]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9546
[gitlab-ce!10319]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10319
[gitlab-ce!12069]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12069
[gitlab-ce!13884]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13884
[gitlab-ce!14882]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14882
