#!/bin/bash

URL=$1
# get a good name we use for storing analysed MR,
# https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10319 becomes gitlab-ce-10319
NAME=$(echo "${URL}"| sed "s#/merge_requests/#/#g" | rev | cut -d "/" -f 1-2 | rev | sed "s#/#-#")
HTML_FILE="html/${NAME}.html"
RESULT_FILE="result/${NAME}.md"

# Calculates the size of a selector, trims all the whitespace first
function calc_size {
  pup --file "${HTML_FILE}" --indent 0 "$1" | tr -d ' ' | wc -c
}

# Counts the nodes matching a selector
function count_nodes {
  pup --file "${HTML_FILE}" --number "$1"
}

# Formats bytes as kilobytes, precision of2
function to_kilobyte {
  echo "$(echo "scale=2; $1 / 1024" | bc) kilobyte"
}

# Prints percentage
function percent {
  echo "scale=2; $1 / $2 * 100" | bc
}

###
#
# SCRIPT starts really here:
#
###

## Download the page, save it for later analysis

echo "Downloading ${URL}"
curl "${URL}" -o "${HTML_FILE}"

## Re-download the three times to calculate time to first byte

echo "Calculating average time to first byte"
TMP_FILE=".${NAME}.tmp"
rm -f "${TMP_FILE}"
touch "${TMP_FILE}"
seq 3 | xargs -Iz curl "${URL}" -w '%{time_starttransfer}\n' -o /dev/null >> "${TMP_FILE}"
TTFB=$(awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }' "${TMP_FILE}")
rm -rf "${TMP_FILE}"

# Calculate base-line, size of <body>
echo "Analysis with pup"
BODY_SIZE=$(calc_size body)

# Print a byte_count compared to body size
function size {
  echo -e "\t$(to_kilobyte "$1") ($(percent "$1" "${BODY_SIZE}")% of body)"
}


NOTES="ul#notes-list > li.note" # Selector for all notes
SYSTEM_NOTE="ul#notes-list > li.system-note" # Selector for system notes (commit lists, mentions, etc.)
COMMIT="ul#notes-list > li.system-note .gfm-commit, ul#notes-list > li.system-note .gfm-commit-range" # Selector for commits in system notes
DISCUSSION="ul#notes-list > li.note-discussion" # Selector for discussions

# Selector for discussions that are NOT resolved, unfortunately pup does not allow .a.b
UNRESOLVED_DISCUSSION="ul#notes-list > li.note-discussion .discussion-body:not(.hide)"

# Printing of analysis result
function print_result {
  echo "# Analysis of ${URL}"

  echo "- File info: \`$(ls -lah "${HTML_FILE}")\`"
  echo "- Average Time to first Byte: ${TTFB} seconds"
  echo -e "\n* * *\n"

  NOTES_SIZE=$(calc_size "${NOTES}")
  NOTES_COUNT=$(count_nodes "${NOTES}")

  echo "- \`<body>\` size: $(size "${BODY_SIZE}") with ${NOTES_COUNT} notes:"

  SYSTEM_NOTE_SIZE=$(calc_size "${SYSTEM_NOTE}")
  SYSTEM_NOTE_COUNT=$(count_nodes "${SYSTEM_NOTE}")
  echo -e "     - \`${SYSTEM_NOTE}\` size: $(size "${SYSTEM_NOTE_SIZE}") - ${SYSTEM_NOTE_COUNT} elements"

  DISCUSSION_SIZE=$(calc_size "${DISCUSSION}")
  DISCUSSION_COUNT=$(count_nodes "${DISCUSSION}")
  echo -e "     - \`${DISCUSSION}\` size: $(size "${DISCUSSION_SIZE}") - ${DISCUSSION_COUNT} elements"
  USER_SIZE=$(echo "${NOTES_SIZE} - ${SYSTEM_NOTE_SIZE} - ${DISCUSSION_SIZE}" | bc)
  USER_COUNT=$(echo "${NOTES_COUNT} - ${SYSTEM_NOTE_COUNT} - ${DISCUSSION_COUNT}" | bc)
  echo -e "     - User Comments size: $(size "${USER_SIZE}") - ${USER_COUNT} elements # there is no real selector that works with pup, the values are calculated with (.li.note - discussion - system notes)"

  echo -e "\n* * *\n"

  echo "## Potential savings:"

  echo "- Only rendering 3 commits per commit list "
  COMMIT_COUNT=$(count_nodes "${COMMIT}")
  COMMIT_LIST_COUNT=$(count_nodes "ul#notes-list > li.system-note .note-body a[data-original=\"Compare with previous version\"]")
  MAX_COMMITS=$(echo "${COMMIT_LIST_COUNT} * 3" | bc)
  if [[ ${COMMIT_COUNT} -le ${MAX_COMMITS} ]]; then
    echo -e "    ... would have no effect, there less commits than (commit lists * 3) (${COMMIT_COUNT} < ${MAX_COMMITS})"
  else
    COMMIT_SIZE=$(calc_size "${COMMIT}")
    COMMIT_SAVING_RAW=$(echo "((${COMMIT_COUNT} - ${MAX_COMMITS}) * ${COMMIT_SIZE} / ${COMMIT_COUNT} )" | bc)
    COMMIT_SAVING=$(percent "${COMMIT_SAVING_RAW}" "${SYSTEM_NOTE_SIZE}")
    echo -e "    ... would only render a maximum ${MAX_COMMITS} of ${COMMIT_COUNT} commits, saving $(to_kilobyte "${COMMIT_SAVING_RAW}") (${COMMIT_SAVING}% of all \`${SYSTEM_NOTE}\`)"
  fi

  echo "- Only rendering unresolved discussions (\`${UNRESOLVED_DISCUSSION}\`)... "
  UNRESOLVED_COUNT=$(count_nodes "${UNRESOLVED_DISCUSSION}")
  if [[ ${UNRESOLVED_COUNT} -eq ${DISCUSSION_COUNT} ]]; then
    echo -e "    ... would have no effect, as there are no resolved discussions"
  else
    UNRESOLVED_SIZE=$(calc_size "${UNRESOLVED_DISCUSSION}")
    RESOLVED_SIZE=$(echo "${DISCUSSION_SIZE} - ${UNRESOLVED_SIZE}" | bc)
    RESOLVED_COUNT=$(echo "${DISCUSSION_COUNT} - ${UNRESOLVED_COUNT}" | bc)
    RESOLVED_SAVING=$(percent "${RESOLVED_SIZE}" "${DISCUSSION_SIZE}")
    echo -e "    ... would leave ${RESOLVED_COUNT} of ${DISCUSSION_COUNT} unrendered, saving $(to_kilobyte "${RESOLVED_SIZE}") (${RESOLVED_SAVING}% of all \`${DISCUSSION}\`)"
  fi

}

echo -e "\n- - -"
print_result | tee "${RESULT_FILE}"

echo -e "\n- - -\nSaved analyis to ${RESULT_FILE}"
